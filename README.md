# Dotfiles
Config files for Vim, Tmux, Zsh.

## Quickstart
Execute command below:
```bash
bash -c "$(curl -fsSL https://gitlab.com/filip.wiechec/dotfiles/raw/master/setup.sh)"
```
